package vetkoli.sanket.poccustomview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<LatLang> points = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final CircleView circleView = (CircleView) findViewById(R.id.cv_mandi);
        circleView.setCentre(new LatLang(18.561, 73.923, "You are here"));

        LatLang mumbai = new LatLang(19.07, 72.88, "Mumbai");
        LatLang karjat = new LatLang(18.92, 73.33, "Karjat");
        LatLang satara = new LatLang(17.68, 74.02, "Satara");
        LatLang ahmadnagar = new LatLang(19.09, 74.75, "Ahmadnagar");
        points.add(mumbai);
        points.add(karjat);
        points.add(satara);
        points.add(ahmadnagar);
        circleView.setPoints(points);

        circleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        float x = event.getX();
                        float y = event.getY();
                        circleView.userTouched(x, y);
                }
                return false;
            }
        });
    }
}
