package vetkoli.sanket.poccustomview;

/**
 * Created by Sanket on 1/4/17.
 */

public class Point {

    private float x;
    private float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
