package vetkoli.sanket.poccustomview;

import android.location.Location;

/**
 * Created by Sanket on 31/3/17.
 */

public class LatLang {

    private Location location;
    private String locationName;
    private Point point;

    public LatLang(double latitude, double longitude, String name) {
        this.location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        this.locationName = name;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setPoint(float x, float y) {
        this.point = new Point(x, y);
    }

    public Point getPoint() {
        return point;
    }

    //dividing by 1000 since distance is returned in metres and is expected in km
    public float getDistanceInKmTo(LatLang latLang) {
        return location.distanceTo(latLang.location)/1000;
    }

    //adding 90 degrees since bearingTo() returns angle relative to north pole (Y-axis)
    public float getAngleBetween(LatLang latLang) {
        return location.bearingTo(latLang.location) + 90;
    }
}
