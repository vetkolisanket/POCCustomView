package vetkoli.sanket.poccustomview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;

/**
 * Created by Sanket on 1/4/17.
 */

public class MarkerView extends RelativeLayout {

    private TextView tvContent;
    private WeakReference<CircleView> circleViewWeakReference;

    private MarkerView(Context context, int layoutResource) {
        super(context);
        setupLayoutResource(layoutResource);
        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    public static MarkerView getInstance(Context context) {
        return new MarkerView(context, R.layout.custom_marker_view);
    }

    /**
     * Sets the layout resource for a custom MarkerView.
     *
     * @param layoutResource
     */
    private void setupLayoutResource(int layoutResource) {

        View inflated = LayoutInflater.from(getContext()).inflate(layoutResource, this);

        inflated.setLayoutParams(new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        inflated.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

        // measure(getWidth(), getHeight());
        inflated.layout(0, 0, inflated.getMeasuredWidth(), inflated.getMeasuredHeight());

    }


    public void setCircleView(CircleView circleView) {
        circleViewWeakReference = new WeakReference<>(circleView);
    }
}
