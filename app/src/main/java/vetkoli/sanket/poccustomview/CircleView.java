package vetkoli.sanket.poccustomview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Sanket on 31/3/17.
 */

public class CircleView extends View {

    public final int DEFAULT_OUTER_CIRCLE_RADIUS = 20;
    public final int DEFAULT_CIRCLE_COLOR = Color.RED;
    private final float STROKE_WIDTH = 5;
    public final int DEFAULT_POINT_RADIUS = 10;
    public final int DEFAULT_CENTRE_POINT_COLOR = Color.GREEN;
    public final int DEFAULT_POINT_COLOR = Color.BLUE;

    private int center_x;
    private int center_y;

    private int outerCircleRadius;
    private int innerCircleRadius;
    private int pointRadius;
    private int circleColor;

    private LatLang centre;
    private List<LatLang> points;
    private LatLang selectedPoint;

    private Paint centrePaint;
    private Paint nearbyLocationsPaint;
    private Paint circlePaint;
    private Paint linePaint;

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        getCustomAttributes(attrs);

        initCentrePaint();
        initCirclePaint();
        initLocationPaint();
        initLinePaint();
    }

    private void getCustomAttributes(@Nullable AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CircleView);
        outerCircleRadius = typedArray.getInt(R.styleable.CircleView_cv_outer_circle_radius, DEFAULT_OUTER_CIRCLE_RADIUS);
        circleColor = typedArray.getColor(R.styleable.CircleView_cv_outer_circle_color, DEFAULT_CIRCLE_COLOR);
        innerCircleRadius = typedArray.getInt(R.styleable.CircleView_cv_inner_circle_radius, DEFAULT_OUTER_CIRCLE_RADIUS);
        pointRadius = typedArray.getInt(R.styleable.CircleView_cv_point_radius, DEFAULT_POINT_RADIUS);
        typedArray.recycle();
    }

    private void initLinePaint() {
        linePaint = new Paint();
        linePaint.setColor(DEFAULT_POINT_COLOR);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(STROKE_WIDTH);
        linePaint.setStrokeJoin(Paint.Join.ROUND);
        linePaint.setStrokeCap(Paint.Cap.ROUND);
        linePaint.setPathEffect(new DashPathEffect(new float[] {10,20}, 0));
    }

    private void initCentrePaint() {
        centrePaint = new Paint();
        centrePaint.setAntiAlias(true);
        centrePaint.setColor(DEFAULT_CENTRE_POINT_COLOR);
    }

    private void initCirclePaint() {
        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setStrokeWidth(STROKE_WIDTH);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeJoin(Paint.Join.ROUND);
        circlePaint.setStrokeCap(Paint.Cap.ROUND);
        circlePaint.setColor(circleColor);
        circlePaint.setPathEffect(new DashPathEffect(new float[] {10, 10}, 0));
    }

    private void initLocationPaint() {
        nearbyLocationsPaint = new Paint();
        nearbyLocationsPaint.setAntiAlias(true);
        nearbyLocationsPaint.setColor(DEFAULT_POINT_COLOR);
    }

    public void setPoints(List<LatLang> points) {
        this.points = points;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        computeCanvasCentre();

        drawCircles(canvas);

        drawCentre(canvas);

        drawNearbyLocations(canvas);

        drawSelectedPointsLine(canvas);
    }

    private void drawSelectedPointsLine(Canvas canvas) {
        if (selectedPoint != null) {
            canvas.drawLine(center_x, center_y, selectedPoint.getPoint().getX(),
                    selectedPoint.getPoint().getY(), circlePaint);
            drawMarker(canvas, selectedPoint);
        }
    }

    private void drawNearbyLocations(Canvas canvas) {
        if (points != null) {
            centrePaint.setColor(DEFAULT_POINT_COLOR);
            int size = points.size();
            for (int i = 0; i < size; i++) {
                LatLang point = points.get(i);
                float distance = centre.getDistanceInKmTo(point);
                float bearing = point.getAngleBetween(centre);
                float x = (float) (center_x + Math.cos(Math.toRadians(bearing)) * distance);
                float y = (float) (center_y + Math.sin(Math.toRadians(bearing)) * distance);
                canvas.drawCircle(x, y, DEFAULT_POINT_RADIUS, nearbyLocationsPaint);
                point.setPoint(x, y);
            }
        }
    }

    private void drawCentre(Canvas canvas) {
        if (centre != null) {
            centrePaint.setColor(DEFAULT_CENTRE_POINT_COLOR);
            canvas.drawCircle(center_x, center_y, pointRadius, centrePaint);
            centre.setPoint(center_x, center_y);
            drawMarker(canvas, centre);
        }
    }

    private void computeCanvasCentre() {
        int width = getWidth();
        int height = getHeight();

        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int usableWidth = width - (paddingLeft + paddingRight);
        int usableHeight = height - (paddingTop + paddingBottom);

        center_x = paddingLeft + (usableWidth / 2);
        center_y = paddingTop + (usableHeight / 2);
    }

    private void drawCircles(Canvas canvas) {
        canvas.drawCircle(center_x, center_y, outerCircleRadius, circlePaint);
        canvas.drawCircle(center_x, center_y, innerCircleRadius, circlePaint);
    }

    private void drawMarker(Canvas canvas, LatLang latLang) {
        MarkerView markerView = MarkerView.getInstance(getContext());
        int width = markerView.getWidth();
        int height = markerView.getHeight();
        int stateId = canvas.save();
        canvas.translate(latLang.getPoint().getX() - width/2, latLang.getPoint().getY() - height);
        ((TextView) markerView.findViewById(R.id.tvContent)).setText(latLang.getLocationName());
        markerView.draw(canvas);
        canvas.restoreToCount(stateId);
    }

    public void userTouched(float x, float y) {
        int size = points.size();
        for (int i = 0; i < size; i++) {
            LatLang point = points.get(i);
            if (computeDistance(point, x, y) < DEFAULT_POINT_RADIUS * 5) {
                Toast.makeText(getContext(), point.getLocationName(), Toast.LENGTH_SHORT).show();
                selectedPoint = point;
                invalidate();
                break;
            }
        }
    }

    private float computeDistance(LatLang point, float x, float y) {
        return (float) Math.sqrt(
                Math.pow(x - point.getPoint().getX(), 2) +
                        Math.pow(y - point.getPoint().getY(), 2)
        );
    }

    public void setCentre(LatLang latLang) {
        centre = latLang;
    }
}


